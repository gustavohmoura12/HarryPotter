//
//  ViewController.swift
//  HarryPotter
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Personagens : Decodable {
    let actor: String
    let name: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var atores : [Personagens]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return atores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MinhaCelula
        let ator = atores![indexPath.row]
        
        cell.Personagem.text = ator.name
        cell.Ator.text = ator.actor

        cell.imagem.kf.setImage(with: URL(string: ator.image))
        
        return cell
    }
    
    @IBOutlet weak var tableView_personagens: UITableView!
    
    
    private func requestData(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Personagens].self){
            response in
            if let atores_group = response.value{
                self.atores = atores_group
            }
            self.tableView_personagens.reloadData()
        }
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        requestData()
        tableView_personagens.dataSource = self
    }

    
    
    private func setData(){}

}

